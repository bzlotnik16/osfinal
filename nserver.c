#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <dirent.h>
#include <errno.h>

#define SERVER_PORT 1051 // CHANGE THIS!
#define BUF_SIZE 256

// We make this a global so that we can refer to it in our signal handler
int serverSocket; 

/*
  We need to make sure we close the connection on signal received, otherwise we have to wait 
  for server to timeout.
*/
void closeConnection() {
  printf("\nClosing Connection with file descriptor: %d \n", serverSocket);
  close(serverSocket);
  exit(1);
}

int main(int argc, char *argv[]) {
    int connectionToClient, bytesReadFromClient;
   
    // These are the buffers for sending data to the client and receiving
    char sendLine[BUF_SIZE];
    char receiveLine[BUF_SIZE];
    
    // Create a server socket
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in serverAddress;
    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family      = AF_INET;
    
    // INADDR_ANY means we will listen to any address
    // htonl and htons converts address/ports to network formats
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddress.sin_port        = htons(SERVER_PORT);
    
    // Bind to port
    if (bind(serverSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) == -1) {
      printf("Unable to bind to port just yet, perhaps the connection has to be timed out\n");
      exit(-1);
    }

    // Before we listen, register for Ctrl+C being sent so we can close our connection
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = closeConnection;
    sigIntHandler.sa_flags = 0;

    sigaction(SIGINT, &sigIntHandler, NULL);
    
    // Listen and queue up to 10 connections
    listen(serverSocket, 10);

    //Define variables that I believe I need
    char fakein[32], exit2[32], create[32], readin[32], delete[32], new[32], str[12], nullTerm[32];
    long party;
    int result = 0, cresult = 0, parts = -1;
    char *lines[3];
    FILE *fp;

    strcpy(fakein, "no");
    strcpy(exit2, "return");
    strcpy(create, "create");
    strcpy(readin, "read");
    strcpy(delete, "delete");
    strcpy(nullTerm, "\n");
    
    while (1) {
        /*
          Accept connection (this is blocking)
          2nd parameter you can specify connection
          3rd parameter is for socket length
        */
        connectionToClient = accept(serverSocket, (struct sockaddr *) NULL, NULL);
        
        // Read the request that the client has
        while ( (bytesReadFromClient = read(connectionToClient, receiveLine, BUF_SIZE)) > 0) {
            // Need to put a NULL string terminator at end
            receiveLine[bytesReadFromClient] = 0;
            
            size_t len = strlen(receiveLine); //Fix \n at the end of the string
            if (len > 0 && receiveLine[len-1] == '\n') {
	        receiveLine[--len] = '\0';
	    }

            // Show what client sent
            printf("Received: %s\n", receiveLine);

            int i = 0; //Chop it up by spaces into an array
            char *p = strtok (receiveLine, " ");
            char *lines[4];
            lines[2] = "-1";
            while (p != NULL) {
                lines[i++] = p;
                p = strtok (NULL, " ");
            }

            party = strtol(lines[2], NULL, 10);
            parts = (int) party;
            char cwd[PATH_MAX];
	    getcwd(cwd, sizeof(cwd));
            
	    if (strcmp(lines[0], create) == 0) { //Menu
                DIR * dir = opendir("fileData");
		if (!dir) {
		  mkdir("fileData", 0777);
		}
		char directory[80];
                char num[80], vex[32];
                  
	        for (int i = 0; i < parts; i++) {
                  strcpy(directory, cwd);
                  strcat(directory, "/fileData/");
                  strcpy(new, directory);
                  sprintf(num, "part%d", i);
                  strcat(new, num);
                  //printf("I'm partition: %d, making directory: %s\n", i, new);
                  dir = opendir(new);
		  if (!dir) {
		    mkdir(new, 0777);
                    //printf("I'm partition: %d, making directory: %s\n", i, new);
		  }
                  //printf("I'm partition: %d, using directory: %s\n", i, new);
		  strcat(new, "/");
	          strcat(new, lines[1]);
		  strcat(new, ".txt\n");
		  strcpy(vex, cwd);
		  strcat(vex, "/fileData/");
		  strcat(vex, lines[1]);
		  strcat(vex, ".txt");
		  fp = fopen(vex, "a+");
		  fputs(new, fp);
		  fclose(fp);
		  //printf("vex = %s, cwd = %s, directory = %s, new = %s\n", vex, cwd, directory, new);
                }
	      cresult = 1;
	      int count = 0;
              while (( (bytesReadFromClient = read(connectionToClient, receiveLine, BUF_SIZE)) > 0)){ // && (cresult != 0)) {
                // Need to put a NULL string terminator at end
                strcpy(nullTerm, "\n");
		receiveLine[bytesReadFromClient] = 0;
                cresult = strcmp(receiveLine, nullTerm);
		//printf("Got: %s Null: %s Also: %d\n", receiveLine, nullTerm, cresult);
	        if (cresult == 0) {
		  break;
		} else {
		  if (count >= parts) {
                    count = 0;
		  }
                  strcpy(directory, cwd);
                  strcat(directory, "/fileData/");
                  strcpy(new, directory);
                  sprintf(num, "part%d", count);
                  strcat(new, num);
		  strcat(new, "/");
	          strcat(new, lines[1]);
		  strcat(new, ".txt");
		  fp = fopen(new, "a+");
		  fputs(receiveLine, fp); //user input
		  fclose(fp);
		  count++;
		}
	      }
	      printf("Your file has been created.\n");

            } else if (strcmp(lines[0], readin) == 0) { //Read
              printf("You read file '%s'\n", lines[1]);
              int count = 0, lineNum = 0, checks = 0;
              if (parts >= 0) {
	        lineNum = parts;
		char cwd[PATH_MAX], directory[80];
                getcwd(cwd, sizeof(cwd));
                strcpy(directory, cwd);
                strcat(directory, "/fileData/");
                strcat(directory, lines[1]);
	        strcat(directory, ".txt");
                fp = fopen(directory, "r");
	        char line[256];
	        FILE*fpp;
                while (fgets(line, sizeof line, fp) != NULL) {
                  if (count == lineNum) {
		    size_t len = strlen(line); //Fix \n at the end of the string
                    if (len > 0 && line[len-1] == '\n') {
	              line[--len] = '\0';
	            }
		    fpp = fopen(line, "r");
                    char linner[256];
                    while (fgets(linner, sizeof linner, fpp) != NULL) {
		      printf("%s", linner);
		    }
		    fclose(fpp);
		    count++;
          	  } else {
		    count++;
		  }
                }
		fclose(fp);
              } else {
	      while (checks == 0) {
		char cwd[PATH_MAX], directory[80];
                getcwd(cwd, sizeof(cwd));
                strcpy(directory, cwd);
                strcat(directory, "/fileData/");
                strcat(directory, lines[1]);
	        strcat(directory, ".txt");
                fp = fopen(directory, "r");
	        char line[256];
	        FILE*fpp;
                while (fgets(line, sizeof line, fp) != NULL) {
	        
                  size_t len = strlen(line); //Fix \n at the end of the string
                  if (len > 0 && line[len-1] == '\n') {
	            line[--len] = '\0';
	          }
		  fpp = fopen(line, "r");
                  char linner[256];
		  count = 0;
                  while (fgets(linner, sizeof linner, fpp) != NULL) {
		    if (count == lineNum) {
		      printf("%s", linner);
		      fclose(fpp);
		    } else {
		      count++;   
	            }
          	  }
                }
		if (lineNum != count) {
		  checks = 1;
		}
                lineNum++;
		fclose(fp);
	      }
	      }
            } else if (strcmp(lines[0], delete) == 0) { //Delete 
		char cwd[PATH_MAX], directory[80];
                getcwd(cwd, sizeof(cwd));
                strcpy(directory, cwd);
                strcat(directory, "/fileData/");
                strcat(directory, lines[1]);
	        strcat(directory, ".txt");
                fp = fopen(directory, "r");
	        char line[256];
	        FILE*fpp;
                while (fgets(line, sizeof line, fp) != NULL) {
                  size_t len = strlen(line); //Fix \n at the end of the string
                  if (len > 0 && line[len-1] == '\n') {
	            line[--len] = '\0';
	          }
		  remove(line);
		}
		fclose(fp);
		remove(directory);
	        printf("You deleted file '%s'\n", lines[1]); 
            }
            strcpy(fakein, receiveLine);
            result = strcmp(fakein, exit2);
        
            
            // Print text out to buffer, and then write it to client (connectionToClient)
            // snprintf(sendLine, sizeof(sendLine), "Hello Client\n");
            write(connectionToClient, sendLine, strlen(sendLine));
            
            // Zero out the receive line so we do not get artifacts from before
            bzero(&receiveLine, sizeof(receiveLine));
            if (result == 0) {
                close(connectionToClient);
            }
        }
    }
}



